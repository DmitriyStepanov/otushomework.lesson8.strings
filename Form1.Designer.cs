﻿
namespace StringsAndRegularExpressions
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.butSetUrl = new System.Windows.Forms.Button();
            this.tbUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbAdress = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.buReset = new System.Windows.Forms.Button();
            this.butLoad = new System.Windows.Forms.Button();
            this.wb = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // butSetUrl
            // 
            this.butSetUrl.Location = new System.Drawing.Point(34, 80);
            this.butSetUrl.Name = "butSetUrl";
            this.butSetUrl.Size = new System.Drawing.Size(128, 23);
            this.butSetUrl.TabIndex = 0;
            this.butSetUrl.Text = "Послать запрос";
            this.butSetUrl.UseVisualStyleBackColor = true;
            this.butSetUrl.Click += new System.EventHandler(this.butSetUrl_Click);
            // 
            // tbUrl
            // 
            this.tbUrl.Location = new System.Drawing.Point(34, 41);
            this.tbUrl.Name = "tbUrl";
            this.tbUrl.Size = new System.Drawing.Size(663, 20);
            this.tbUrl.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Строка запроса к сайту";
            // 
            // lbAdress
            // 
            this.lbAdress.FormattingEnabled = true;
            this.lbAdress.Location = new System.Drawing.Point(37, 147);
            this.lbAdress.Name = "lbAdress";
            this.lbAdress.Size = new System.Drawing.Size(660, 251);
            this.lbAdress.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 128);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Найденные адреса картинок";
            // 
            // buReset
            // 
            this.buReset.Location = new System.Drawing.Point(575, 80);
            this.buReset.Name = "buReset";
            this.buReset.Size = new System.Drawing.Size(122, 23);
            this.buReset.TabIndex = 5;
            this.buReset.Text = "Сброс";
            this.buReset.UseVisualStyleBackColor = true;
            this.buReset.Click += new System.EventHandler(this.buReset_Click);
            // 
            // butLoad
            // 
            this.butLoad.Location = new System.Drawing.Point(724, 354);
            this.butLoad.Name = "butLoad";
            this.butLoad.Size = new System.Drawing.Size(122, 23);
            this.butLoad.TabIndex = 5;
            this.butLoad.Text = "Скачать";
            this.butLoad.UseVisualStyleBackColor = true;
            this.butLoad.Click += new System.EventHandler(this.butLoad_Click);
            // 
            // wb
            // 
            this.wb.Location = new System.Drawing.Point(724, 41);
            this.wb.MinimumSize = new System.Drawing.Size(20, 20);
            this.wb.Name = "wb";
            this.wb.Size = new System.Drawing.Size(379, 292);
            this.wb.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 411);
            this.Controls.Add(this.wb);
            this.Controls.Add(this.butLoad);
            this.Controls.Add(this.buReset);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbAdress);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbUrl);
            this.Controls.Add(this.butSetUrl);
            this.Name = "Form1";
            this.Text = "GetImagesFromWeb";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button butSetUrl;
        private System.Windows.Forms.TextBox tbUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbAdress;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buReset;
        private System.Windows.Forms.Button butLoad;
        private System.Windows.Forms.WebBrowser wb;
    }
}

