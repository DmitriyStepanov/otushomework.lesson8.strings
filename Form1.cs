﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StringsAndRegularExpressions
{
    public partial class Form1 : Form
    {
        private Worker worker { get; set; }

        private ObservableCollection<string> ImagesUrl { get; set; }
        
        public Form1()
        {
            InitializeComponent();

            worker = new Worker();

            this.ImagesUrl = new ObservableCollection<string>();

            this.lbAdress.SelectedIndexChanged += LbAdress_SelectedIndexChanged;

        }

        private void LbAdress_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.lbAdress.SelectedItem != null)
            {
                var uri = ((ListBox)sender).SelectedItem.ToString();

                this.wb.Url = new Uri(uri);
            }
        }   

        private void butSetUrl_Click(object sender, EventArgs e)
        {
            try
            {
                worker.SetUrl(this.tbUrl.Text);

                worker.GetHtml();

                foreach(var b in worker.GetImagesUri())
                {
                    this.ImagesUrl.Add(b);
                }

                this.lbAdress.DataSource = this.ImagesUrl;

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void butLoad_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lbAdress.SelectedItem == null)
                    throw new Exception("Ничего не выбрано");


                FolderBrowserDialog dial = new FolderBrowserDialog();

                dial.Description = "Путь для сохранения файла";

                string fileName = string.Empty;

                if(dial.ShowDialog() == DialogResult.OK)
                {
                    var data = worker.DownLoad(this.lbAdress.SelectedItem.ToString(), ref fileName);

                    File.WriteAllBytes(Path.Combine(dial.SelectedPath, fileName), data);
                }

                MessageBox.Show("Загрузка завершена", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
        }

        private void buReset_Click(object sender, EventArgs e)
        {
            worker.Reset();

            this.lbAdress.DataSource = null;
        }
    }
}
