﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StringsAndRegularExpressions
{
    public class Worker
    {
        public string Url { get; set; }

        private string HtmlString { get; set; }

        private string Origin { get; set; }

        public bool IsUrlSet { get; private set; }

        public bool IsHtmlReceived { get; private set; }

        

        public Worker()
        { }

        public Worker(string url)
        {
            this.SetUrl(url);
        }

        public void SetUrl(string url)
        {

            Regex exp = new Regex(@"https{0,1}:\/\/\S{3,}");

            if (!exp.IsMatch(url))
                throw new ArgumentException("Введенная строка не является действительным адресом web-сайта");
            
            this.Url = url;

            this.SetOrigin();

            this.IsUrlSet = true;
        }

        public void GetHtml()
        {

            if (!this.IsUrlSet)
                throw new Exception("Адрес сайта не установлен");
            
            WebRequest request = WebRequest.Create(this.Url);

            WebResponse response = request.GetResponse();

            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    this.HtmlString = reader.ReadToEnd();

                    this.IsHtmlReceived = true;
                }
            }

        }

        public List<string> GetImagesUri()
        {
            var result = new List<string>();
            
            if (!this.IsHtmlReceived)
                throw new Exception("Необходимо выполнить запрос к сайту");

            
            //Поиск всех элементов с тегом img
            var exp = new Regex(@"src=[\u0022|\']\S+(jpg|gif|png|bmp)[\u0022|\']");
            var matches = exp.Matches(this.HtmlString);

            var images = new List<string>();
            
            foreach(Match m in matches) 
            {
                var buffer = m.Value.Substring(0, m.Value.Length - 1);

                if (buffer.Contains("http"))
                {
                    result.Add(buffer.Substring(5));
                }
                else
                {
                    result.Add(this.Origin + buffer.Substring(5));
                }
                
            }

            return result;
        }

        public void Reset()
        {
            this.IsHtmlReceived = false;

            this.IsUrlSet = false;

            this.Url = string.Empty;

            this.HtmlString = string.Empty;

            this.Origin = string.Empty;

        }

        private void SetOrigin()
        {
            Regex exp = new Regex(@"https{0,1}:\/\/\S{3,}.(ru|com|org)");

            Match m = exp.Match(this.Url);

            if (string.IsNullOrEmpty(m.Value))
                throw new Exception("Домен сайта должен находиться в зоне .ru, .com или .org");

            this.Origin = m.Value;
        }
        
        public byte[] DownLoad(string url, ref string fileName)
        {
            if (string.IsNullOrEmpty(url))
                throw new Exception("Строка запроса не установлена");

            WebClient client = new WebClient();

            var index = url.LastIndexOf("/");

            fileName = url.Substring(index + 1, url.Length - index - 1);

            return client.DownloadData(url);
        }
    }
 }
